package com.thpa.a9019.graphdesign;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomSwip extends PagerAdapter {
    private int[] imageResources = {R.drawable.graph1, R.drawable.graph2, R.drawable.graph3};
    private Context context;
    private LayoutInflater inflater;

    public CustomSwip(Context cx) {
        context = cx;
    }

    @Override
    public int getCount() {

            return imageResources.length;

    }


    @Override
    public Object instantiateItem( ViewGroup container, int position) {
        inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.activity_custom_swip,container,false);
        ImageView imageView = (ImageView)itemView.findViewById(R.id.swipe_image_View);
        TextView textView = (TextView)itemView.findViewById(R.id.txt_image_count);
        imageView.setImageResource(imageResources[position]);
        position++;
        container.addView(itemView);
        if(position == 1){
            textView.setText("กราฟแบบเส้น (Line Chart) ");
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent getGraph1 = new Intent(context, MainActivity.class);
                    context.startActivity(getGraph1);
                }
            });
        }
        else if(position == 2){
            textView.setText("กราฟแบบแท่ง (Bar Chart) ");
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent getGraph2 = new Intent(context,BarChartMain.class);
                    context.startActivity(getGraph2);
                }
            });
        }
        else {
            textView.setText("Pi Chart");
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent getGraph3 = new Intent(context,PiChartMain.class);
                    context.startActivity(getGraph3);
                }
            });

        }

        return itemView;


    }



    @Override
    public void destroyItem( ViewGroup container, int position,  Object object) {

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }
}
