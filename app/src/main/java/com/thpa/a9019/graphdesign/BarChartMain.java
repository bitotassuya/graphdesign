package com.thpa.a9019.graphdesign;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import javax.xml.transform.Templates;

public class BarChartMain extends AppCompatActivity {
    private BarChart mChart;
    private EditText valueX, valueY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_chart);

        //Initial Value
        mChart = (BarChart) findViewById(R.id.Bar_Chart);
        valueX = (EditText) findViewById(R.id.x_value_bar);
        valueY = (EditText) findViewById(R.id.y_value_bar);
        mChart.getDescription().setEnabled(false);

        setData(10);
        mChart.setFitBars(true);


    }

    private void setData(int count) {
        ArrayList<BarEntry> yValue = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            float value = (float) (Math.random() * 100);
            yValue.add(new BarEntry(i, (int) value));
        }
        BarDataSet barDataSet = new BarDataSet(yValue, "Data set");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setDrawValues(true);

        BarData data = new BarData(barDataSet);
        mChart.setData(data);
        mChart.invalidate();
        mChart.animateY(500);

    }
}
