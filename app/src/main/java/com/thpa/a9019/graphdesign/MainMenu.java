package com.thpa.a9019.graphdesign;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.relex.circleindicator.CircleIndicator;

public class MainMenu extends AppCompatActivity {
    private ViewPager viewPager;
    private CustomSwip customSwip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        viewPager = (ViewPager)findViewById(R.id.view_Pager);
        customSwip = new CustomSwip(this);
        viewPager.setAdapter(customSwip);


        //Circle indicatoe
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);

    }
}
